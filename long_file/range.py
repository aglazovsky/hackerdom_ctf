#!/usr/bin/python

import urllib2
import re

def main():
	host ="http://long.training.hackerdom.ru"
	user_agent = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64)'
	range_bytes = "0"
	
	while len(range_bytes) > 0:
		r = urllib2.Request(host, None, {'User-Agent' : user_agent, 'Range':'bytes='+range_bytes+'-'+str(int(range_bytes) +100) })
		response = urllib2.urlopen(r).read()
		try:
			resp_text = re.findall("[ a-zA-Z0-9\-'?]+!!!", response)
			range_bytes = re.findall("\d+", resp_text[0])[0]
			print(resp_text)
			print(range_bytes)
		except:
			range_bytes = ""
			print response

if __name__ == '__main__':
	main()
