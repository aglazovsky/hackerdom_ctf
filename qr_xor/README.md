To decrypt image we should know structure of BMP file header.
[Link]http://www.fastgraph.com/help/bmp_header_format.html

Then we can craft by hand original header of encrypted file and XOR with encrypted image, we should get key.
Also we shoud have some knowledge or guess image hight and width.
Once we know key we can succesfully decrypt file.

$ xortool-xor -f s -s "\x42\x4d\x36\x8c\x0a\x00\x00\x00\x00\x00\x36\x00\x00\x00\x28\x00\x00\x00\xe0\x01\x00\x00\xe0\x01\x00\x00\x01\x00\x18\x00\x00\x00\x00\x00\x00\x8c" -n|xxd  -l 36 -g 72 -c 66
where s is header of encrypted BMP file
