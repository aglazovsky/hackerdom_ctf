#!/usr/bin/python

from __future__ import print_function
import os
import sys
import struct 

def main():
#[start, size, comment]
	bm_struct = [ [0, 2, 'signature, must be 4D42 hex'], [2, 4, 'size of BMP file in bytes (unreliable)'], [6, 2, 'reserved, must be zero'], [8, 2, 'reserved, must be zero'], [10, 4, 'offset to start of image data in bytes'], [14, 4, 'size of BITMAPINFOHEADER structure, must be 40'], [18, 4, 'image width in pixels'], [22, 4, 'image height in pixels'], [26, 2, 'number of planes in the image, must be 1'], [28, 2, 'number of bits per pixel (1, 4, 8, or 24)'], [30, 4, 'compression type (0=none, 1=RLE-8, 2=RLE-4)'],  [34, 4, 'size of image data in bytes (including padding)'] ]
	p = lambda x,y : print('%d \t %16s \t  %s' % (struct.unpack('>'+ len(x)/2*'H', x)[0], ' '.join('%02x' % ord(b) for b in x), y) )
	file_name = sys.argv[1]
	file = open(file_name,'rb')
	for i in bm_struct:
		file.seek(i[0])
		byte = file.read(i[1])
#	for i in xrange(1,36):
#		file.seek(i)
#		byte = file.read(1)
#		print("%x" %  ord(byte))
	#f = lambda x: print x
#	for i in(byte):
	#	print(byte)
	#	print ('debug: %d' % len(byte))
		p(byte, i[2])
	file.close()		

if __name__ == '__main__':
	main()
