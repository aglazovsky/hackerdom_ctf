#!/ush/share/python
import hashlib


def main():
	l = [line.rstrip('\n') for line in open('sha256.txt') ]
	for line in l:
		hash_obj = hashlib.sha256()
		hash2 = hashlib.sha256()
		hash_obj.update(line)	
		hash2.update(hash_obj.hexdigest())
		if hash2.hexdigest() == 'd5c9e1c7f862d634a759a56ed97549f62a6d78401440a5c09cfbf7dfc20e8190':
			print line
			print hash2.hexdigest()
if __name__ == '__main__':
	main()
